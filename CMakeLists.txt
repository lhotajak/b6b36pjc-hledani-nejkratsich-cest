cmake_minimum_required(VERSION 3.17)
project(path_finder)

set(CMAKE_CXX_STANDARD 14)
add_executable(path_finder main.cpp graph_creator.cpp graph_creator.h path_finder.cpp path_finder.h algorithms.cpp algorithms.h result.cpp result.h graph.h fast_input.cpp fast_input.h)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(path_finder Threads::Threads)
