//
// Created by jakub on 13.01.2021.
//

#include <thread>
#include <bits/stdc++.h>
#include "algorithms.h"

#define INF INT_MAX

/* Bellman–Ford algorithm */
// single run of BF algo
void bellmanFordSingleRun(int N, int E, const int *from, const int *to, const int *cost,
                          int *dist, int *pred, int startNode);

std::vector<Result> BF::solve(Graph &g) {
    int N = g.getN(), E = g.getE();
    int from[E], to[E], cost[E];
    int ptr = 0;

    // Init values
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            if (g.cost(i, j) != 0) {
                from[ptr] = i;
                to[ptr] = j;
                cost[ptr] = g.cost(i, j);
                ++ptr;
            }
        }
    }


    // thread function for BF
    auto thread_func_BF = [&N, &E, &from, &to, &cost](int fromIdx, int toIdx,
                                                      std::vector<Result> *tmpRes) { // <fromIdx, toIdx-1>
        int dist[N], pred[N];
        for (int i = fromIdx; i < toIdx; ++i) {
            bellmanFordSingleRun(N, E, from, to, cost, dist, pred, i);
            for (int j = 0; j < N; ++j) {
                if (i == j || dist[j] == INF) continue;
                // create path
                int u = j;
                std::vector<int> path = {u};
                while (u != i) {
                    u = pred[u];
                    path.push_back(u);
                }
                std::reverse(path.begin(), path.end());
                // push to resultList
                tmpRes->emplace_back(i, j, dist[j], path);
            }
        }
    };

    // multithreading
    int partSize = N / nThreads; // how many vertexes per thread
    std::thread threadsBF[nThreads]; // threadpool
    std::vector<std::vector<Result>> vecOfResList(nThreads);
    for (int i = 0; i < nThreads - 1; ++i) { // call threads and give them vectors to fill
        threadsBF[i] = std::thread(thread_func_BF, i * partSize, (i + 1) * partSize, &(vecOfResList[i]));
    }
    threadsBF[nThreads - 1] = std::thread(thread_func_BF, (nThreads - 1) * partSize, N, &(vecOfResList[nThreads - 1]));
    for (int i = 0; i < nThreads; ++i) threadsBF[i].join(); // join everything

    // merge result lists
    std::vector<Result> resList;
    for (auto &res : vecOfResList) {
        resList.insert(resList.end(), res.begin(), res.end());
    }
    return resList;
}

// https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm
void bellmanFordSingleRun(int N, int E, const int *from, const int *to, const int *cost,
                          int *dist, int *pred, int startNode) {
    for (int i = 0; i < N; ++i) {
        dist[i] = INF;
        pred[i] = -1;
    }
    dist[startNode] = 0;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < E; ++j) {
            if (dist[from[j]] == INF || cost[j] == INF) continue;
            int u = from[j], v = to[j];
            if (dist[u] + cost[j] < dist[v]) {
                dist[v] = dist[u] + cost[j];
                pred[v] = u;
            }
        }
    }
}




/* Floyd-Warshall algorithm */
// https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm
std::vector<Result> FW::solve(Graph &g) {
    // Initialization
    int N = g.getN();
    int *dist = new int[N * N]; // 1D array instead of 2D (dist[i * N + j] ~ dist[i][j])
    int *next = new int[N * N];
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            dist[i * N + j] = INF;
            next[i * N + j] = -1;
        }
    }
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            int c = g.cost(i, j);
            if (c != 0) {
                dist[i * N + j] = c;
                next[i * N + j] = j;
            }
        }
    }
    for (int i = 0; i < N; ++i) {
        dist[i * N + i] = 0;
        next[i * N + i] = i;
    }

    // FW
    for (int k = 0; k < N; ++k) {
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                int iN = i * N;
                if (dist[iN + k] != INF && dist[k * N + j] != INF) { // check for infinity, skip if infinity
                    if (dist[iN + j] > dist[iN + k] + dist[k * N + j]) {
                        dist[iN + j] = dist[iN + k] + dist[k * N + j];
                        next[iN + j] = next[iN + k];
                    }
                }
            }
        }
    }

    // Result
    std::vector<Result> resList;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            if (dist[i * N + j] != 0 && dist[i * N + j] != INF) {
                // build path
                int u = i;
                std::vector<int> path = {u};
                while (u != j) {
                    u = next[u * N + j];
                    path.push_back(u);
                }
                // push result
                resList.emplace_back(i, j, dist[i * N + j], path);
            }
        }
    }
    // release memory
    delete[] dist;
    delete[] next;
    return resList;
}
