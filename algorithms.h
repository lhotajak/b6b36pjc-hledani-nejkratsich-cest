//
// Created by jakub on 13.01.2021.
//

#ifndef SEMESTRAL_PROJ_ALGORITHMS_H
#define SEMESTRAL_PROJ_ALGORITHMS_H

#include <vector>
#include "result.h"
#include "graph.h"

// Base class for finding shortest paths
class ShortestPathsAlgorithm {
public:
    virtual std::vector<Result> solve(Graph &g) = 0;

    virtual ~ShortestPathsAlgorithm() = default;
};

// Bellman-Ford
class BF : public ShortestPathsAlgorithm {
public:
    BF() : nThreads(1) {}

    explicit BF(int numOfThreads) : nThreads(numOfThreads) {}

    std::vector<Result> solve(Graph &g) override;

    ~BF() override = default;

private:
    int nThreads;
};

// Floyd-Warshall
class FW : public ShortestPathsAlgorithm {
public:
    std::vector<Result> solve(Graph &g) override;

    ~FW() override = default;
};


#endif //SEMESTRAL_PROJ_ALGORITHMS_H
