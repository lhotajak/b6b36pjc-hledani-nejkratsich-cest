#include "fast_input.h"

FastInput::FastInput(std::istream &inStream) : inStream(inStream) {}

int FastInput::nextInt() {
    int ret = 0;
    int c = inStream.get();
    while (c <= ' ')
        c = inStream.get();
    bool neg = c == '-';
    if (neg)
        c = inStream.get();
    do {
        ret = ret * 10 + c - '0';
    } while ((c = inStream.get()) >= '0' && c <= '9');
    if (neg)
        return -ret;
    return ret;
}

long FastInput::nextLong() {
    long ret = 0;
    int c = inStream.get();
    while (c <= ' ')
        c = inStream.get();
    bool neg = c == '-';
    if (neg)
        c = inStream.get();
    do {
        ret = ret * 10 + c - '0';
    } while ((c = inStream.get()) >= '0' && c <= '9');
    if (neg)
        return -ret;
    return ret;
}

double FastInput::nextDouble() {
    double ret = 0, div = 1;
    int c = inStream.get();
    while (c <= ' ')
        c = inStream.get();
    bool neg = c == '-';
    if (neg)
        c = inStream.get();
    do {
        ret = ret * 10 + c - '0';
    } while ((c = inStream.get()) >= '0' && c <= '9');
    if (c == '.') {
        while ((c = inStream.get()) >= '0' && c <= '9') {
            ret += (c - '0') / (div *= 10);
        }
    }
    if (neg)
        return -ret;
    return ret;
}

void FastInput::nextLine(std::string &str) {
    getline(inStream, str);
}

