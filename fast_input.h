#ifndef FASTINPUT_H
#define FASTINPUT_H

#include <istream>
#include <string>

/*
 * Fast input functions for parsing numbers from text
 */

class FastInput {
public:
    explicit FastInput(std::istream &inStream);

    int nextInt();

    long nextLong();

    double nextDouble();

    void nextLine(std::string &str);

private:
    std::istream &inStream;
};


#endif //FASTINPUT_H
