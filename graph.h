//
// Created by jakub on 13.01.2021.
//

#ifndef SEMESTRAL_PROJ_GRAPH_H
#define SEMESTRAL_PROJ_GRAPH_H


#include <vector>
#include <iostream>

/*
 * Simple representation of graph
 * Adjacency matrix
 */

class Graph {
public:
    Graph(int nNodes, int nEdges) : N(nNodes), E(nEdges) {
        data = new int[nNodes * nNodes];
    }

    int cost(int i, int j) const { return data[i * N + j]; }

    void setCost(int i, int j, int cost) { data[i * N + j] = cost; }

    int getN() const { return N; }

    int getE() const { return E; }

    void printGraph() {
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                std::cout << data[i * N + j] << " ";
            }
            std::cout << "\n";
        }
    }

    ~Graph() {
        delete[] data;
    }

private:
    int N, E;
    int *data;
};


#endif //SEMESTRAL_PROJ_GRAPH_H
