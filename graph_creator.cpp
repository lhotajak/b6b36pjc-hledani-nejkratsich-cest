//
// Created by jakub on 12.01.2021.
//

#include <vector>
#include <unordered_set>
#include <algorithm>
#include <random>
#include "graph_creator.h"

#define MIN_VAL 1

struct MyEdge {
    int from, to;

    MyEdge(int from, int to) {
        this->from = from;
        this->to = to;
    }
};

// random int value from interval [min, max]
int getRandomIntValue(int min, int max);

// creates adjacency matrix and prints it to given ostream
void create_graph(int N, int E, int MAX_VAL, std::ostream &out) {
    srand(time(nullptr)); // init rand

    // create empty adjacency matrix
    std::vector<std::vector<int>> adj_matrix(N);
    for (int i = 0; i < N; ++i) {
        adj_matrix[i].resize(N, 0);
    }

    // create a vector of all possible Edges
    std::vector<MyEdge> possibleEdges;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            if (i != j) possibleEdges.emplace_back(i, j);
        }
    }

    // shuffle the vector and take first E elements
    std::shuffle(possibleEdges.begin(), possibleEdges.end(), std::mt19937{std::random_device{}()});
    for (int i = 0; i < E; ++i) {
        const MyEdge &e = possibleEdges[i];
        adj_matrix[e.from][e.to] = getRandomIntValue(MIN_VAL, MAX_VAL);
    }

    out << N << " " << E << "\n";
    for (int i = 0; i < N; ++i) {
        out << adj_matrix[i][0];
        for (int j = 1; j < N; ++j) {
            out << " " << adj_matrix[i][j];
        }
        out << "\n";
    }
}

int getRandomIntValue(int min, int max) {
    return min + rand() % (max - min + 1);
}
