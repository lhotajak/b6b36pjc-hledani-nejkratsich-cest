//
// Created by jakub on 12.01.2021.
//

#ifndef SEMESTRAL_PROJ_GRAPH_CREATOR_H
#define SEMESTRAL_PROJ_GRAPH_CREATOR_H

#include <ostream>

// creates adjacency matrix and prints it to given ostream
void create_graph(int N, int E, int MAX_VAL, std::ostream &out);

#endif //SEMESTRAL_PROJ_GRAPH_CREATOR_H
