#include <cerrno>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include "graph_creator.h"
#include "path_finder.h"

#define NUMBER_INPUT_ERROR -1
#define MAX_THREADS 16

// print --help message
void printHelpMsg();

// c-like string to non-negative number
int strToInt(const std::string &str);

// this is the entry point of the whole program
int main(int argc, char *argv[]) {
    bool wrongInput = false;

    // Help
    if (argc == 2) {
        std::string str(argv[1]);
        if (str == "--help") {
            printHelpMsg();
            return 0;
        }
    }

    // Graph generator
    if (argc == 5 || argc == 6) {
        std::string graph(argv[1]);
        if (graph == "--graph") {
            int N = strToInt(argv[2]);
            int E = strToInt(argv[3]);
            int MAX_VAL = strToInt(argv[4]);
            if (MAX_VAL < 1 || N <= 0 || E < 0 || E > N * (N - 1)) {
                std::cout << "Wrong graph parameters (MAX_VAL: " << MAX_VAL << ", N: " << N << ", E: " << E << ")\n\n";
                printHelpMsg();
                return 1;
            } else {
                if (argc == 6) {
                    std::string outputFile = std::string(argv[5]);
                    std::ofstream out;
                    out.open(outputFile);
                    create_graph(N, E, MAX_VAL, out);
                } else {
                    create_graph(N, E, MAX_VAL, std::cout);
                }
                return 0;
            }
        }
    }

    // Shortest path solver
    std::ifstream inStream;
    std::ofstream outStream;
    bool measureExecTime = false;
    int numOfThreads = 1;
    std::string algorithm = "BF";

    // Parse arguments
    bool useInFileStream = false, useOutFileStream = false;
    for (int i = 1; i < argc; ++i) {
        std::string str(argv[i]);
        int strLen = str.size();
        if (str.substr(0, 8) == "--input=") {
            if (str.size() > 8) {
                inStream.open(str.substr(8, strLen-8));
                useInFileStream = true;
            } else {
                wrongInput = true;
                break;
            }
        } else if (str.substr(0, 9) == "--output=") {
            if (str.size() > 9) {
                outStream.open(str.substr(9, strLen-9));
                useOutFileStream = true;
            } else {
                wrongInput = true;
                break;
            }
        } else if (str == "--time") {
            measureExecTime = true;
        } else if (str.substr(0, 10) == "--threads=") {
            int N = strToInt(str.substr(10, strLen-10));
            if (N > 0 && N <= MAX_THREADS) {
                numOfThreads = N;
            } else {
                std::cout << "Number of threads must be between 1 and " << MAX_THREADS << " (" << N
                          << " was given)\n\n";
                wrongInput = true;
                break;
            }
        } else if (str.substr(0, 12) == "--algorithm=") {
            std::string alg = str.substr(12, strLen-12);
            if (alg == "BF" || alg == "FW") {
                algorithm = alg;
            } else {
                std::cout << "Unknown algorithm: " << alg << "\n\n";
                wrongInput = true;
            }
        } else {
            std::cout << "Unknown argument: " << str << "\n\n";
            wrongInput = true;
            break;
        }
    }

    // FW runs only with 1 thread
    if (numOfThreads > 1 && algorithm == "FW") {
        std::cout << "When FW is chosen as solving algorithm, --threads must be set to 1\n\n";
        wrongInput = true;
    }

    if (wrongInput) { // If there was some problem while parsing
        printHelpMsg();
        return 1;
    } else { // Run solver
        std::istream *in;
        std::ostream *out;
        if (useInFileStream) in = &inStream;
        else in = &std::cin;
        if (useOutFileStream) out = &outStream;
        else out = &std::cout;
        findShortestPaths(*in, *out, measureExecTime, numOfThreads, algorithm);
    }
    return 0;
}


int strToInt(const std::string &str) {
    const char *cStr = str.c_str();
    char *endPtr;
    errno = 0;
    long long_var = strtol(cStr, &endPtr, 0);
    // out of range, extra junk at end, no conversion at all
    if (errno == ERANGE || *endPtr != '\0' || cStr == endPtr) {
        return NUMBER_INPUT_ERROR;
    }
    return (int) long_var;
}

void printHelpMsg() {
    std::cout << "Usage:\n";
    std::cout << "Help:\n";
    std::cout << "path_finder --help\n\n";

    std::cout << "Path finder:\n";
    std::cout
            << "path_finder [--input=<path/to/input_file>] [--output=<path/to/output_file>] [--algorithm=<algorithm>] "
               "[--time] [--threads=<N>]\n";
    std::cout << "algorithm -> which algorithm should be used? (BF/FW)\n";
    std::cout << "BF -> Bellman–Ford algorithm, FW -> Floyd–Warshall algorithm\n";
    std::cout << "N -> number of threads (only for BF)\n\n";

    std::cout << "Graph generator:\n";
    std::cout << "path_finder --graph <N> <E> [<path/to/output_file>]\n";
    std::cout << "N -> number of vertices, E -> number of edges\n";
    std::cout << "It must hold, that E <= N*(N-1)\n";
}
