//
// Created by jakub on 13.01.2021.
//

#include <iostream>
#include <chrono>
#include "path_finder.h"
#include "algorithms.h"
#include "fast_input.h"

// creates graf structure in memory from given istream
Graph *createGraph(std::istream &istream);

// runs solver with given algorithm and graph
std::vector<Result> runSolver(Graph &graph, ShortestPathsAlgorithm &alg);

template<typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

bool findShortestPaths(std::istream &inStream,
                       std::ostream &outStream,
                       bool measureExecTime,
                       int numOfThreads,
                       std::string &alg) {
    auto start = std::chrono::high_resolution_clock::now();

    // create graph
    Graph *g = createGraph(inStream);

    // choose algorithm
    ShortestPathsAlgorithm *a;
    if (alg == "BF") a = new BF(numOfThreads);
    else if (alg == "FW") a = new FW;
    else return false;

    auto end = std::chrono::high_resolution_clock::now();
    if (measureExecTime)
        std::cout << "Time - graph loading: " << to_ms(end - start).count() << " ms.\n";
    start = std::chrono::high_resolution_clock::now();

    // get list of results
    std::vector<Result> results = runSolver(*g, *a);

    // release memory
    delete a;
    delete g;

    end = std::chrono::high_resolution_clock::now();
    if (measureExecTime)
        std::cout << "Time - solving: " << to_ms(end - start).count() << " ms.\n";
    start = std::chrono::high_resolution_clock::now();

    // print result to ostream
    for (const auto &r: results) {
        outStream << "from: " << r.from
                  << ", to: " << r.to
                  << ", cost: " << r.cost
                  << ", path: " << r.pathAsString() << "\n";
    }


    end = std::chrono::high_resolution_clock::now();
    if (measureExecTime)
        std::cout << "Time - printing result: " << to_ms(end - start).count() << " ms.\n";

    return true;
}

std::vector<Result> runSolver(Graph &graph, ShortestPathsAlgorithm &alg) {
    return alg.solve(graph);
}

Graph *createGraph(std::istream &istream) {
    FastInput fastInput(istream);
    int N = fastInput.nextInt();
    int E = fastInput.nextInt();
    auto *g = new Graph(N, E);
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            g->setCost(i, j, fastInput.nextInt());
        }
    }
    return g;
}
