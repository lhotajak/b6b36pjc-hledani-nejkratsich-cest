//
// Created by jakub on 13.01.2021.
//

#ifndef SEMESTRAL_PROJ_PATH_FINDER_H
#define SEMESTRAL_PROJ_PATH_FINDER_H


#include <istream>

bool findShortestPaths(std::istream &inStream,
                       std::ostream &outStream,
                       bool measureExecTime,
                       int numOfThreads,
                       std::string &algorithm);

#endif //SEMESTRAL_PROJ_PATH_FINDER_H
