### PJC Semestrální práce
#### Hledání nejkratší cesty mezi všemi vrcholy grafu
#### Jakub Lhoták
https://cw.fel.cvut.cz/b201/courses/b6b36pjc/ukoly/semestralka

#### Popis zadání
Cílem práce bylo vytvořit program napsaný v jazyce C++, který možní pozorovat rozdíl v efektivitě 2 algoritmů, určených k hledání nejkratších cest v orientovaném grafu. Program umožňuje porovnávat efektivitu a způsob vykonávání Floyd–Warshallova algoritmu s opakovaným Bellman–Fordovým algoritmem, který se zavolá z každého vrcholu grafu.

#### Vstup
Orientovaný graf s **N** vrcholy a **E** hranami.

##### Formát vstupu
První řádek vstupu obsahuje 2 celá kladná čísla: **N** (počet rcholů) a **E** (počet hran). Dále následuje **N** řádků. Na **i**-tém řádku (**i** od 0 do **N**-1) se nachází **N** nezáporných celých čísel, které reprezentují cenu hrany z vrcholu s indexem **i** do vrcholů 0 až **N**-1. Pokud je některá z cen rovna 0, znamená to, že z vrcholu s indexem **i** nevede hrana do vrcholu s příslušným indexem. Jedná se tedy o standardní matici sousednosti, která obsahuje ceny jednotlivých hran. Příklad:
```
56 270
0 1 0 ...
2 0 5 ...
5 0 0 ...
. . . 
. . . 
. . . 
```

#### Výstup
Výstupem programu je seznam dvojic indexů vrcholů (**i**, **j**) s cenou nejkratší cesty z vrcholu s indexem **i** do vrcholu s indexem **j** a také seznam jednotlivých vrcholů, skrze které hledaná nejkratší cesta vede. Pokud neexistuje cesta mezi vrcholy, tak na výstupu daný záznam není.

##### Formát výstupu
Příklad:
```
from: 0, to: 1, cost: 16, path: (0 --> 3 --> 2 --> 1)
from: 0, to: 2, cost: 7, path: (0 --> 3 --> 2)
...
```

#### Manuál k programu
Jedná se o program bez grafického rozhrání. Veškerá komunikace probíhá pomocí předávání argumentů při volání programu. Program má 3 funkce:

- **Nápověda** - Vypíše použití programu a skončí.
```
./path_finder.exe --help
```
- **Generování grafu** - Vygeneruje náhodný graf s **N** vrcholy a **E** hranami, ceny jednotlivých hran budou celá čísla v rozmezí od 1 do **MAX_VALUE**. Nepovinným čtvrtým parametrem lze specifikovat, do kterého souboru se má graf uložit.
```
./path_finder.exe --graph N E MAX_VALUE [output_file]
```
- **Hledání nejkratších cest v grafu** - Jako vstup přijme graf ve výše zmíněném formátu a do výstupního proudu vypíše výsledek tak, jak je specifikovaný formát výstupu. Program má celkem 5 volitelných argumentů:
  - **--input=input_file** - ze kterého vstupního souboru se má číst (defaulně standardní vstup)
  - **--output=output_file** - do kterého výstupního souboru se má zapisovat (defaulně standardní výstup)
  - **--algorithm=ALG** - jaký algoritmus se má použít? (**BF** - Bellman-Ford / **FW** - Floyd-Warshall)
  - **--time** - argument přidáme, pokud nás zajímá, kolik času zabírá vykonávání programu
  - **--threads=N** - umožní program spustit s **N** vlákny (pouze pro **BF** algoritmus)
```
./path_finder.exe [--input=<input_file>] [--output=<output_file>] [--algorithm=<ALG>] [--time] [--threads=<N>]
```

#### Popis implementace
Program je rozdělen do několika logických částí. Funkce **main** (nachází v souboru **main.cpp**) je hlavní funkce, která je volána po spuštění programu. V ní probíhá parsování vstupních argumentů a volání dalších funkcí a modulů, které zajišťují fungování programu. 

Pokud chceme vygenerovat graf, volá funkce **main** funkci **create_graph** (implementovanou v souboru **graph_creator.cpp**). Graf se generuje tím způsobem, že se nejdříve vygenerují všechny možné hrany, které mohou spojovat libovolné 2 vrcholy v grafu. Z nich se poté vybere **E** náhodných hran, ke kterým se přiřadí náhodná cena v zadaném rozmezí.

Co se týče řešení úlohy nejkratších cest, vytvořil jsem si třidu **ShortestPathsAlgorithm**, která je abstraktní a pouze obsahuje metodu **solve**. Z této třídy dědí 2 další třídy: **BF** a **FW**. Tyto třídy implementují metodu solve, podle toho o jaký algoritmus se jedná. Algoritmy jsem implementoval tak, jak jsou uvedené na stránkách Wikipedie ([Bellman–Ford algorithm](https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm), [Floyd–Warshall algorithm](https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm)). Bellmanův-Fordův algoritmus jsem navíc implementoval tak, že umožňuje vykonávání programu s více paralalně běžícími vlákny najednou.

#### Prostředí
**PC:**
- 64-bit
- i5-7300HQ CPU @ 2.50GHz
- 8 GB RAM

**Kompilátor:**
- Windows 10 (Cygwin): **g++ (GCC) 10.2.0**
- Lubuntu 18.04: **g++ (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0**

Zkompilovaný program se nachází v *bin/path_finder.exe* (Windows) a *bin/path_finder* (Linux).
#### Příklad použití
```
./path_finder.exe --input=../examples/graph8x30.txt --algorithm=FW
```

#### Výsledky měření při testu velkého grafu
Porovnávám výsledky **BF** s **FW** algoritmem nad stejným vstupem. **BF** algoritmus navíc spouštím s 1 až 4 vlákny. Program byl zkompilován v release módu. Testovací graf se nachází v souboru *examples/graph1000x5000.txt*. Jedná se o graf, který má 1000 vrcholů a 5000 hran s cenami od 1 do 100.

Výsledek na operačním systému **Windows 10**:

| Algoritmus | Naměřený čas |
| ------------- | ------------- |
| **BF** - 1 vlákno | 7 378 ms  |
| **BF** - 2 vlákna | 11 301 ms  |
| **BF** - 3 vlákna | 39 362 ms  |
| **BF** - 4 vlákna | 60 989 ms  |
| **FW**  |  1 795 ms |

Časy, které jsou uvedeny v tabulce, jsou hodnoty naměřené čistě během vykonávání konkrétního algoritmu, není tam tedy zahrnut čas načítání vstupu a ukládání výstupu. Je vidět, že Floydův–Warshallův algoritmus, který je přesně vytvořen na tento typ úlohy dosahuje správných výsledků v nižším čase, než opakovaný Bellmanův-Fordův algoritmus.

Co je ovšem velmi nepříjmné překvapení, jsou časy, kterých dosáhly vícevláknové varianty Bellmanova-Fordova algoritmu. Z tabulky naměřených hodnot vyplývá, že čím více vláken poskytnu algoritmu, tím pomaleji dosáhne výsledku! Což je velmi divné, protože počítač, na kterém se časy měřily má čtyřjádrový procesor.

Zkusil jsem provést identický test na stejném počítači, ovšem nyní s operačním systémem Lubuntu 18.04. Výsledky jsou na první pohled úplně jiné.

Výsledek na operačním systému **Lubuntu 18.04**:

| Algoritmus | Naměřený čas |
| ------------- | ------------- |
| **BF** - 1 vlákno | 7 470 ms  |
| **BF** - 2 vlákna | 4 016 ms  |
| **BF** - 3 vlákna | 2 813 ms  |
| **BF** - 4 vlákna | 2 197 ms  |
| **FW**  |  1 765 ms |

Je vidět, že program zkompilovaný pro Linux funguje tak, jak se od něj očekávalo. Floydův-Warshallův algoritmus i jednovláknový Bellmanův-Fordův algoritmus dosahují na Lubuntu stejných časů, ovšem vícevláknové varianty dosahují na Lubuntu mnohem nižších časů, než na Windows. Čtyřvláknová varianta **BF** algoritmu je přibližně 3.4 krát rychlejší, než její jednovláknová varianta, což je velmi znatelné zlepšení.

I přesto, že se podařilo úspěšně paralelizovat Bellmanův-Fordův algoritmus, lepších výsledků i tak dosahuje Floydův-Warshallův algoritmus.

Důvod, proč mi nefunguje vícevláknová varianta **BF** algoritmu na operačním systému Windows 10, nevím. Pravděpodobně bude chyba někde v kompilátoru (Cygwin?).
