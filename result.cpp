//
// Created by jakub on 13.01.2021.
//

#include "result.h"

std::string Result::pathAsString() const {
    std::string pathStr = "(";
    int lastIdx = (int) path.size() - 1;
    for (int i = 0; i < lastIdx; ++i)
        pathStr += std::to_string(path[i]) + " --> ";
    pathStr += std::to_string(path[lastIdx]);
    pathStr += ")";
    return pathStr;
}
