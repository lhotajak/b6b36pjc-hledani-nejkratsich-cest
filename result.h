//
// Created by jakub on 13.01.2021.
//

#ifndef SEMESTRAL_PROJ_RESULT_H
#define SEMESTRAL_PROJ_RESULT_H


#include <utility>
#include <vector>
#include <string>

// class for storing information about a path from one vertex to another
class Result {
public:
    Result(int from, int to, int cost, std::vector<int> path) {
        this->from = from;
        this->to = to;
        this->cost = cost;
        this->path = std::move(path);
    }

    int from, to, cost;
    std::vector<int> path; // sequence of vertexes (indexes)

    // string representation of path
    std::string pathAsString() const;
};


#endif //SEMESTRAL_PROJ_RESULT_H
